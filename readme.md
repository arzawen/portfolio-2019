# [Marion Beyou](Portfolio)

This project was developed with HTML5, Sass, jQuery, and Hammer.js.

Performance basics are covered: assets are minified into single CSS and JS files, and the images are optimized.

## Misc:

* [Bēhance](https://www.behance.net/mbeyou)
* [GitLab](https://gitlab.com/arzawen)
* [Linkedin](www.linkedin.com/in/mbeyou)